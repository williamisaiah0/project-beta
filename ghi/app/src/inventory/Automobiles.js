import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function Automobiles(props) {
    const [automobiles, setAutomobiles] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const url = 'http://localhost:8100/api/automobiles/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setAutomobiles(data.autos)
            }
        }
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
            <h1 className= 'mt-2'>Automobiles</h1>
            <Link to="/automobiles/new" className='col-2 btn btn-primary mt-2 mb-2'>Add Automobile</Link>
                <table className="table table-striped ">
                    <thead>
                        <tr>
                            <th>VIN</th>
                            <th>Color</th>
                            <th>Year</th>
                            <th>Model</th>
                            <th>Manufacturer</th>
                            <th>Sold</th>
                        </tr>
                    </thead>
                <tbody>
                    {automobiles.map(auto => {
                        return (
                        <tr key={auto.id}>
                            <td>{ auto.vin }</td>
                            <td>{ auto.color }</td>
                            <td>{ auto.year }</td>
                            <td>{ auto.model.name }</td>
                            <td>{ auto.model.manufacturer.name }</td>
                            <td>{ auto.sold.toString()}</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default Automobiles;
