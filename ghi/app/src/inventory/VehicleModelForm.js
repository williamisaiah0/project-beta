import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";

function VehicleModelForm() {
    const [manufacturers, setManufacturers] = useState([]);
    const [name, setName] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const navigate = useNavigate();

    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }
    const handleUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }
    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};

        data.name = name;
        data.picture_url = pictureUrl;
        data.manufacturer_id = manufacturer;

        const vehicleUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(vehicleUrl, fetchConfig);
        if (response.ok) {
            const newVehicle = await response.json();

            setName('');
            setPictureUrl('');
            setManufacturer('');

            navigate('/models')
        }
    }

    const fetchData = async() => {
        const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(manufacturersUrl);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1 >Create a Vehicle Model</h1>
                    <form id="create-vehicle-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={handleNameChange} value={name} placeholder="Name" required type="text" name="name" id="name" className="form-control"/>
                            <label htmlFor="name">Model Name...</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handleUrlChange} value={pictureUrl} placeholder="Picture Url" required type="text" name="picture_url" id="picture_url" className="form-control"/>
                            <label htmlFor="picture_url">Picture Url...</label>
                        </div>
                        <div className="mb-3">
                            <select value ={manufacturer}onChange={handleManufacturerChange} required name="manufacturer" id="manufacturer" className="form-select">
                                <option value="">Choose a Manufacturer...</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option value={manufacturer.id} key={manufacturer.id}>
                                            {manufacturer.name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default VehicleModelForm;
