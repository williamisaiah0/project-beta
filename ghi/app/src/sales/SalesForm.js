import React, { useState, useEffect } from 'react';
import { useNavigate } from "react-router-dom";

function SalesForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);

    const [price, setPrice] = useState('');
    const [automobile, setAutomobile] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const navigate = useNavigate();

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }
    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
    }
    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }
    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.price = price;
        data.automobile = automobile;
        data.salesperson = salesperson;
        data.customer = customer;

        const automobileUrl = `http://localhost:8100/api/automobiles/${data.automobile}/`;
        const statusConfig = {
            method: "put",
            body: JSON.stringify({"sold": true}),
            headers: {
                "Content-Type": "application/json"
            }
        };
        const autoResponse = await fetch(automobileUrl, statusConfig);
        if (autoResponse.ok) {
            const soldUpdated = await autoResponse.json()
        }

        const salesUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                "Content-Type": "application/json"
            }
        };
        const salesResponse = await fetch(salesUrl, fetchConfig);
        if (salesResponse.ok) {
            const newSale = await salesResponse.json();

            setPrice('');
            setAutomobile('');
            setSalesperson('');
            setCustomer('');

            navigate('/sales');
        }
    }

    const fetchData = async() => {
        const autosUrl = 'http://localhost:8100/api/automobiles/';
        const autosResponse = await fetch(autosUrl);
        if (autosResponse.ok) {
            const autosData = await autosResponse.json();
            setAutomobiles(autosData.autos.filter(auto => auto.sold!==true))
        }
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const salespeopleResponse = await fetch(salespeopleUrl);
        if (salespeopleResponse.ok) {
            const salespeopleData = await salespeopleResponse.json();
            setSalespeople(salespeopleData.salespeople)
        }
        const customersUrl = 'http://localhost:8090/api/customers/';
        const customersResponse = await fetch(customersUrl);
        if (customersResponse.ok) {
            const customersData = await customersResponse.json();
            setCustomers(customersData.customers)
        }
    }
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Record a new sale</h1>
                    <form id="create-sale-form" onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <select onChange={handleAutomobileChange} value={automobile} required name="automobile" id="automobile" className="form-select">
                                <option value="">Choose an automobile VIN...</option>
                                {automobiles.map(automobile => {
                                    return (
                                        <option key={automobile.vin}>
                                            {automobile.vin}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleSalespersonChange} value = {salesperson}required name="salesperson" id="salesperson" className="form-select">
                                <option value="">Choose a salesperson...</option>
                                {salespeople.map(salesperson => {
                                    return (
                                        <option value = {salesperson.employee_id} key={salesperson.employee_id}>
                                            {salesperson.first_name} {salesperson.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange={handleCustomerChange} value = {customer} required name="customer" id="customer" className="form-select">
                                <option value="">Choose a customer...</option>
                                {customers.map(customer => {
                                    return (
                                        <option value = {customer.id} key={customer.id}>
                                            {customer.first_name} {customer.last_name}
                                        </option>
                                    )
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={handlePriceChange} value={price} placeholder="0" required type="number" name="price" id="price" className="form-control"/>
                            <label htmlFor="price">Price...</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default SalesForm;
