import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function Customers() {
    const [customers, setCustomers] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const url = 'http://localhost:8090/api/customers/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setCustomers(data.customers);
            }
        }
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
            <h1>Customers</h1>
            <Link to="/customers/new" className='col-2 btn btn-primary mb-2 mt-2'>Add Salesperson</Link>
                <table className="table table-striped ">
                    <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Address</th>
                            <th>Phone Number</th>
                        </tr>
                    </thead>
                    <tbody>
                    {customers.map(customer => {
                        return (
                        <tr key={customer.id}>
                            <td>{ customer.first_name }</td>
                            <td>{ customer.last_name }</td>
                            <td>{ customer.address }</td>
                            <td>{ customer.phone_number }</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default Customers;
