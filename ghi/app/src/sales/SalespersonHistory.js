import React, { useState, useEffect } from 'react';

function SalespersonHistory() {
    const [salespeople, setSalespeople] = useState([]);

    const [salesperson, setSalesperson] = useState('');
    const [sales, setSales] = useState([]);


    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }

    useEffect(() => {
        const fetchData = async () => {
            const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
            const salespeopleResponse = await fetch(salespeopleUrl);
            if (salespeopleResponse.ok) {
                const data = await salespeopleResponse.json();
                setSalespeople(data.salespeople);
            }

            const salesUrl = 'http://localhost:8090/api/sales/';
            const salesResponse = await fetch(salesUrl);
            if (salesResponse.ok) {
                const data = await salesResponse.json();
                setSales(data.sales.filter(sale => sale.salesperson.employee_id === parseInt(salesperson)));
            }
        }
        fetchData();
    }, [salesperson]);

    return (
        <div className="my-5 container">
            <h1>Salesperson History</h1>
                <div className="mb-3 mt-3">
                    <select onChange={handleSalespersonChange} required name="salesperson" id="salesperson" className="form-select">
                        <option value="">Choose a Salesperson...</option>
                        {salespeople.map(salesperson => {
                            return (
                                <option key={salesperson.employee_id} value = {salesperson.employee_id}>
                                    {salesperson.first_name} {salesperson.last_name}
                                </option>
                            )
                        })}
                    </select>
                </div>
                <table className="table table-striped ">
                    <thead>
                        <tr>
                            <th>Salesperson </th>
                            <th>Customer</th>
                            <th>VIN</th>
                            <th>Price</th>
                        </tr>
                    </thead>
                    <tbody>
                        {sales.map(sale => {
                            return (
                            <tr key={ sale.id}>
                                <td>{ sale.salesperson.first_name } {sale.salesperson.last_name}</td>
                                <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                                <td>{ sale.automobile.vin }</td>
                                <td>${ sale.price }.00</td>
                            </tr>
                            );
                        })}
                </tbody>
            </table>
        </div>
    )
}
export default SalespersonHistory;
