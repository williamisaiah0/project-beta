import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function Salespeople() {
    const [salespeople, setSalespeople] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const url = 'http://localhost:8090/api/salespeople/';
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setSalespeople(data.salespeople);
            }
        }
        fetchData();
    }, []);

    return (
        <div className="my-5 container">
            <h1>Salespeople</h1>
            <Link to="/salespeople/new" className='col-2 btn btn-primary mt-2 mb-2'>Add Salesperson</Link>
            <table className="table table-striped ">
                <thead>
                    <tr>
                        <th>Employee ID</th>
                        <th>First Name</th>
                        <th>Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map(salesperson => {
                        return (
                        <tr key={salesperson.employee_id}>
                            <td>{ salesperson.employee_id }</td>
                            <td>{ salesperson.first_name }</td>
                            <td>{ salesperson.last_name }</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default Salespeople;
