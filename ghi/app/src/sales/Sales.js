import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

function Sales() {
    const [sales, setSales] = useState([]);

    useEffect(() => {
        const fetchdata = async () => {
            const url = "http://localhost:8090/api/sales/";
            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setSales(data.sales)
            }
        }
        fetchdata();
    }, []);

    return (
        <div className="my-5 container">
            <h1>Sales</h1>
            <Link to="/sales/new" className='col-2 btn btn-primary mt-2 mb-2'>Add Sale</Link>
            <table className="table table-striped ">
                <thead>
                    <tr>
                        <th>Salesperson Employee ID</th>
                        <th>Salesperson Name</th>
                        <th>Customer</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                        <tr key={sale.id}>
                            <td>{ sale.salesperson.employee_id }</td>
                            <td>{ sale.salesperson.first_name } {sale.salesperson.last_name}</td>
                            <td>{ sale.customer.first_name } { sale.customer.last_name }</td>
                            <td>{ sale.automobile.vin }</td>
                            <td>${ sale.price }.00</td>
                        </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
export default Sales;
