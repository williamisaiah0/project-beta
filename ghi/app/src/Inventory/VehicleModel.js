import React from 'react';
import { Link, } from 'react-router-dom';
import { useState, useEffect } from 'react';

export default function VehicleModel() {
    const [models, setModels] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8100/api/models")
            .then(res => res.json())
            .then(data => {
                setModels(data.models);
            })
    }, [])

    return (
        <div>
            <h1 className= 'mt-2'>Vehicle Models</h1>
            <Link to="/models/new" className='col-2 btn btn-primary mt-2 mb-2'>Add Vehicle Model</Link>
            <table className="table">
                <thead>
                    <tr>
                        <th scope="col">Name</th>
                        <th scope="col">Manufacturer</th>
                        <th scope='col'>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(e => {
                        return (
                            <tr key={e.name}>
                                <th>{e.name}</th>
                                <th>{e.manufacturer.name}</th>
                                <th>
                                    <img style={{ width: 300 }} src={e.picture_url} />
                                </th>
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>
    )
}
