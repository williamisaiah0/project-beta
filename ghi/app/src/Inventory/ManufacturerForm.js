import React,{ useState } from 'react'
import { useNavigate } from "react-router-dom";

export default function ManufacturerForm() {
    const [name, setName] = useState('');
    const navigate = useNavigate();

    const handleSubmit = async (e) => {
        e.preventDefault();
        const url = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify({
                name: name
            }),
            headers: { "Content-Type": "application/json" }
        };

        const res = await fetch(url, fetchConfig);
        if (res.ok) {
            setName('');

            navigate('/manufacturers');
        }
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a manufacturer</h1>
                    <form id="create-vehicle-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input onChange={e => setName(e.target.value)} type="text" value={name} className="form-control" placeholder="Manufacturer Name..." />
                            <label htmlFor="manufacturer">Manufacturer Name...</label>
                        </div>
                    <button className='btn btn-primary'>Create</button>
                </form>
            </div>
        </div>
    </div>
    )
}
