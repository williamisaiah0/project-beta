import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import VehicleModelForm from './Inventory/VehicleModelForm';
import Automobiles from './Inventory/Automobiles';
import AutomobileForm from './Inventory/AutomobileForm';
import Manufacturers from './Inventory/Manufacturers';
import ManufacturerForm from './Inventory/ManufacturerForm';
import VehicleModel from './Inventory/VehicleModel';
import SalespersonForm from './sales/SalespersonForm';
import Salespeople from './sales/Salespeople';
import CustomerForm from './sales/CustomerForm';
import Customers from './sales/Customers';
import SalesForm from './sales/SalesForm';
import Sales from './sales/Sales';
import SalespersonHistory from './sales/SalespersonHistory';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path ='models'>
            <Route path = '' element={<VehicleModel />}/>
            <Route path='new' element={<VehicleModelForm />} />
          </Route>
          <Route path='automobiles'>
            <Route path="" element={<Automobiles />} />
            <Route path="new" element={<AutomobileForm />} />
          </Route>
          <Route path='/manufacturers' element={<Manufacturers />} />
          <Route path='/manufacturers/new' element={<ManufacturerForm />} />
          <Route path='salespeople'>
            <Route path='' element={<Salespeople />} />
            <Route path='new' element={<SalespersonForm />} />
          </Route>
          <Route path='customers'>
            <Route path='' element={<Customers />} />
            <Route path='new' element={<CustomerForm />} />
          </Route>
          <Route path='sales'>
            <Route path='' element={<Sales />} />
            <Route path='new' element={<SalesForm />} />
            <Route path='history' element={<SalespersonHistory />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
