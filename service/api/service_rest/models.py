from django.db import models
from django.urls import reverse

# Create your models here.
class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.PositiveIntegerField(unique=True)

    def __str__(self):
        return self.first_name, self.last_name

    def get_api_url(self):
        return reverse("api_technician", kwargs={"pk": self.pk})


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin

class Appointment(models.Model):
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    reason = models.TextField()
    status = models.TextField()
    vin = models.CharField(max_length=17)
    customer = models.CharField(max_length=100)
    technician = models.ForeignKey(Technician, related_name="appointment", on_delete=models.PROTECT)

    def __str__(self):
        return self.vin

    def get_api_url(self):
        return reverse("api_show_service_appointment", kwargs={"pk": self.pk})
