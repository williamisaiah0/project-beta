# CarCar

*CarCar* is an application for managing aspects of an automobile dealership-specifically its inventory, service center, and sales.
<br>

**Team:**

* Michael Liang - Sales Microservice

## Design
![Design Diagram](CarCarDiagram.jpg)

## Instructions to Run
1. Download and install Docker Desktop if you have not already.
2. Open the terminal and `cd` to the directory you wish to clone the respository in.
3. Clone the respository: `git clone https://gitlab.com/williamisaiah0/project-beta.git`, and `cd` into the project directory.
4. Run the following commands:
`docker volume create beta-data`
`docker-compose build`
`docker-compose up`
5. Once all the containers are up and running on Docker, go to `http://localhost:3000` on your browser to use the CarCar application.

## Inventory Microservice

The Inventory microservice allows users to get a list of, create, update, or delete **manufacturers**, **vehicle models**, and **automobiles** within an inventory.

| Model | Fields |
|-------|---------|
| Manufacturer  | name |
| VehicleModel | name, picture_url, manufacturer|
| Automobile | color, year, vin, sold, model|

In order to create a `VehicleModel`, its `Manufacturer` must first be created. Then you can assign the Manufacturer to that VehicleModel, as `manufacturer` is a foreign key in VehicleModel that references a Manufacturer.

In order to create an `Automobile`, a `VehicleModel` of that automobile must first be created. Then you can assign that VehicleModel to that Automobile, as `model` is a foreign key in Automobile that references a VehicleModel.

### Inventory API Documentation

#### 1. Manufacturer

| Action | Method | URL |
|--------|--------|-----|
| List Manufacturers | GET | `http://localhost:8100/api/manufacturers/` |
| Get a specific Manufacturer | GET | `http://localhost:8100/api/manufacturers/:id/` |
| Create a Manufacturer | POST | `http://localhost:8100/api/manufacturers/` |
| Delete a Manufacturer | DELETE | `http://localhost:8100/api/manufacturers/:id/`|
| Update a Manufacturer | PUT | `http://localhost:8100/api/manufacturers/:id/` |


<details>
<summary>GET: Returns a list of all manufacturers. The returning JSON is formatted as such:
</summary>
<br>

```
{
    "manufacturers": [
        {
            "href": "/api/manufacturers/1/",
            "id": 1,
            "name": "BMW Group"
        },
        {
            "href": "/api/manufacturers/4/",
            "id": 4,
            "name": "Honda Co."
        },
	]
}
```
</details>
<br>
<details>
<summary>GET: To get a specific manufacturer you must include its ID in the URL.
</summary>
<br>

`http://localhost:8100/api/manufacturers/:id`

The information about the specific manufacturer will be returned.
```
{
    "href": "/api/manufacturers/1/",
    "id": 1,
    "name": "BMW Group"
},
```
</details>
<br>
<details>
<summary>POST: To create a manufacturer, your JSON request body should include the name of the manufacturer you want to create:
</summary>
<br>

```
{
    "name": "Toyota Co."
}
```
Following the request, the manufacturer's href, id, and name will be returned.

```
{
    "href": "/api/manufacturers/5/",
    "id": 5,
    "name": "Toyota Co."
}
```
</details>
<br>
<details>
<summary>DELETE: To delete a manufacturer, you must include its id in the URL.
</summary>
<br>

`http://localhost:8100/api/manufacturers/:id/`

If successfully deleted, the name and id with the value of null will be returned.

```
{
    "id": null,
    "name": "Toyota Co."
}
```
</details>
<br>
<details>
<summary>PUT: To update a manufacturer, your URL should include its ID, and the JSON request body should include the new name of the manufacturer you want to update:
</summary>
<br>

`http://localhost:8100/api/manufacturers/:id/`

```
{
    "name": "Toyota Co. 2"
}
```
Following the request, the manufacturer's href, id, and the updated name will be returned.

```
{
    "href": "/api/manufacturers/5/",
    "id": 5,
    "name": "Toyota Co. 2"
}
```
</details>

#### 2. VehicleModel

| Action | Method | URL |
|--------|--------|-----|
| List VehicleModels | GET | `http://localhost:8100/api/models/` |
| Get a specific VehicleModel | GET | `http://localhost:8100/api/models/:id/` |
| Create a VehicleModel | POST | `http://localhost:8100/api/models/` |
| Delete a VehicleModel | DELETE | `http://localhost:8100/api/models/:id/` |
| Update a VehicleModel | PUT | `http://localhost:8100/api/models/:id/` |


<details>
<summary>GET: Returns a list of vehicle models and their information. The returning JSON is formatted as such:
</summary>
<br>

```
{
    "models": [
        {
            "href": "/api/models/1/",
            "id": 1,
            "name": "BMW X3",
            "picture_url": "https://www.bmwusa.com/content/dam/bmwusa/common/vehicles/2022/my23/x3/core-models/mobile/BMW-MY23-X3-DetailPage-Core-MakeitYours-Mobile.jpg",
            "manufacturer": {
                "href": "/api/manufacturers/1/",
                "id": 1,
                "name": "BMW Group"
            }
        },
        {
            "href": "/api/models/2/",
            "id": 2,
            "name": "Sebring",
            "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
            "manufacturer": {
                "href": "/api/manufacturers/1/",
                "id": 1,
                "name": "Chrysler"
            }
        }
    ]
}
```
</details>
<br>
<details>
<summary>GET: To get a specific vehicle model you must include its ID in the URL.
</summary>
<br>

`http://localhost:8100/api/models/:id`

The information about the specific vehicle model will be returned.
```
{
    "href": "/api/models/1/",
    "id": 1,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Daimler-Chrysler"
    }
}
```
</details>
<br>
<details>
<summary>POST: To create a vehicle model, your JSON request body should include the name, picture_url, and the id of its manufacturer.
</summary>
<br>

```
{
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer_id": 1
}
```
Following the request, the vehicle model's information will be returned.

```
{
    "href": "/api/models/2/",
    "id": 2,
    "name": "Sebring",
    "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
    "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "BMW Group"
    }
}
```
</details>
<br>
<details>
<summary>DELETE: To delete a vehicle model, you must include its id in the URL.
</summary>
<br>

`http://localhost:8100/api/models/:id/`

If successfully deleted, the vehicle model's information will be returned, with its ID value as null.

```
{
    "id": null,
    "name": "Civic",
    "picture_url": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQnZbofr82PvTduL7PAihzwu7zT9A0YwvCOHOA9_3L5dg&usqp=CAU&ec=48600112",
    "manufacturer": {
        "href": "/api/manufacturers/4/",
        "id": 4,
        "name": "Honda Co."
    }
}
```
</details>
<br>
<details>
<summary>PUT: To update a vehicle model, your URL should include its ID, and the JSON request body should include the updated name and/or picture_url, and an id of its manufacturer:
</summary>
<br>

`http://localhost:8100/api/models/:id/`

```
{
  "name": "Civic X",
  "picture_url": "https://updatedphoto.com/somecar/",
  "manufacturer_id": 1
}

```
Following the request, the vehicle model's updated information will be returned.

```
{
    "id": 3,
    "name": "Civic X",
    "picture_url": "https://updatedphoto.com/somecar/",
    "manufacturer": {
        "href": "/api/manufacturers/1/",
        "id": 1,
        "name": "Honda Co."
    }
}
```
</details>

#### 3. Automobile

| Action | Method | URL |
|--------|--------|-----|
| List Automobiles | GET | `http://localhost:8100/api/automobiles/` |
| Get a specific Automobile | GET | `http://localhost:8100/api/automobiles/:vin/` |
| Create an Automobile | POST | `http://localhost:8100/api/automobiles/` |
| Delete an Automobile | DELETE | `http://localhost:8100/api/automobiles/:vin/`|
| Update an Automobile | PUT | `http://localhost:8100/api/automobiles/:vin/` |


<details>
<summary>GET: Returns a list of automobiles and its information. The returning JSON is formatted as such:
</summary>
<br>

```
{
    "autos": [
        {
            "href": "/api/automobiles/1A2B3C/",
            "id": 1,
            "color": "Black",
            "year": 2023,
            "vin": "1A2B3C",
            "model": {
                "href": "/api/models/1/",
                "id": 1,
                "name": "BMW X3",
                "picture_url": "https://di-uploads-pod23.dealerinspire.com/bmwofowingsmills/uploads/2023/02/IMG_05281.jpg",
                "manufacturer": {
                    "href": "/api/manufacturers/1/",
                    "id": 1,
                    "name": "BMW Group"
                }
            },
            "sold": true
        },
        {
            "href": "/api/automobiles/1C3CC5FB2AN120174/",
            "id": 4,
            "color": "red",
            "year": 2012,
            "vin": "1C3CC5FB2AN120174",
            "model": {
                "href": "/api/models/1/",
                "id": 1,
                "name": "BMW X3",
                "picture_url": "https://di-uploads-pod23.dealerinspire.com/bmwofowingsmills/uploads/2023/02/IMG_05281.jpg",
                "manufacturer": {
                    "href": "/api/manufacturers/1/",
                    "id": 1,
                    "name": "BMW Group"
                }
            },
            "sold": false
        }
    ]
}
```
</details>
<br>
<details>
<summary>GET: To get a specific automobile, you must include its vin in the URL.
</summary>
<br>

`http://localhost:8100/api/automobiles/:vin/`

The specific automobile's information will be returned.
```
{
    "href": "/api/automobiles/1A2B3C/",
    "id": 1,
    "color": "Black",
    "year": 2023,
    "vin": "1A2B3C",
    "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "BMW X3",
        "picture_url": "https://di-uploads-pod23.dealerinspire.com/bmwofowingsmills/uploads/2023/02/IMG_05281.jpg",
        "manufacturer": {
            "href": "/api/manufacturers/1/",
            "id": 1,
            "name": "BMW Group"
        }
    },
    "sold": true
},
```
</details>
<br>
<details>
<summary>POST: To create an automobile, your JSON request body should include the automobile's color, year, vin, and ID of its vehicle model.
</summary>
<br>

```
{
    "color": "red",
    "year": 2012,
    "vin": "1C3CC5FB2AN120174",
    "model_id": 1
}
```
Following the request, the automobile's information will be returned.

```
{
    "href": "/api/automobiles/1C3CC5FB2AN120174/",
    "id": 4,
    "color": "red",
    "year": 2012,
    "vin": "1C3CC5FB2AN120174",
    "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "BMW X3",
        "picture_url": "https://di-uploads-pod23.dealerinspire.com/bmwofowingsmills/uploads/2023/02/IMG_05281.jpg",
        "manufacturer": {
            "href": "/api/manufacturers/1/",
            "id": 1,
            "name": "BMW Group"
        }
    },
	"sold": false
}
```
</details>
<br>
<details>
<summary>DELETE: To delete an automobile, you must include its vin in the URL.
</summary>
<br>

`http://localhost:8100/api/automobiles/:vin/`

If successfully deleted, the automobile's information will be returned, with its ID value as null.

```
{
    "href": "/api/automobiles/1C3CC5FB2AN120174/",
    "id": null,
    "color": "red",
    "year": 2012,
    "vin": "1C3CC5FB2AN120174",
    "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "BMW X3",
        "picture_url": "https://di-uploads-pod23.dealerinspire.com/bmwofowingsmills/uploads/2023/02/IMG_05281.jpg",
        "manufacturer": {
            "href": "/api/manufacturers/1/",
            "id": 1,
            "name": "BMW Group"
        }
    },
"sold": false
}
```
</details>
<br>
<details>
<summary>PUT: To update an automobile, your URL should include its vin, and the JSON request body should include an updated color, year, and/or sold value.
</summary>
<br>

`http://localhost:8100/api/models/:id/`

```
{
    "color": "grey",
    "year": 2020,
    "sold": true
}


```
Following the request, the automobile's updated information will be returned.

```
{
    "href": "/api/automobiles/1C3CC5FB2AN120174/",
    "id": 4,
    "color": "grey",
    "year": 2020,
    "vin": "1C3CC5FB2AN120174",
    "model": {
        "href": "/api/models/1/",
        "id": 1,
        "name": "BMW X3",
        "picture_url": "https://di-uploads-pod23.dealerinspire.com/bmwofowingsmills/uploads/2023/02/IMG_05281.jpg",
        "manufacturer": {
            "href": "/api/manufacturers/1/",
            "id": 1,
            "name": "BMW Group"
        }
    },
	"sold": true
}
```
</details>


## Sales Microservice

The Sales microservice keeps records of the sales of automobiles from the inventory. Users are able to get a list of, create, or delete a **Sale**, a **Salesperson**, or a **Customer**.

| Model | Fields |
|-------|---------|
| Sale  | price, salesperson, customer, autmobile|
| Salesperson | first_name, last_name, employee_id|
| Customer | first_name, last_name, address, phone_number|
| AutomobileVO | vin |

The `Sale`'s `salesperson`, `customer`, and `automobile` fields are foreign keys to their respective models `Salesperson`, `Customer`, and `AutomobileVO`.

The `AutomobileVO` model is a **value object** that respresents an `Automobile` model of the **Inventory** microservice. Every 60 seconds, the poller within the Sales Microservice pulls the VIN data of the automobiles from the Inventory microservice, and updates the VIN data of the AutomobileVOs.

Once an automobile is selected through the sale form and processed, that automobile will be marked as sold and cannot be resold.

### Sales API Documentation

#### 1. **Salesperson**

| Action | Method | URL |
|--------|--------|-----|
| List Salespeople | GET | `http://localhost:8090/api/salespeople/` |
| Create a Salesperson | POST | `http://localhost:8090/api/salespeople/` |
| Delete a Salesperson | DELETE | `http://localhost:8090/api/salespeople/:employee_id/` |

<details>
<summary> GET: Returns a list of salespeople and their information. The returning JSON is formatted as such:
</summary>
<br>

```
{
    "salespeople": [
        {
            "employee_id": 1,
            "first_name": "Michael",
            "last_name": "Liang"
        },
        {
            "employee_id": 2,
            "first_name": "Other",
            "last_name": "Salesperson"
        }
    ]
}
```
</details>
<br>
<details>
<summary>POST: To create a salesperson, you must include the first name and last name in the request body.
</summary>
<br>

```
{
    "first_name": "Michael",
    "last_name": "Liang",
}
```
Following this request, a JSON response will be returned in a similar format, with the addition of an auto generated employee ID of the salesperson you just created.

```
{
    "employee_id": 1,
    "first_name": "Michael",
    "last_name": "Liang",
}
```
</details>
<br>
<details>
<summary>DELETE: To delete a salesperson, you must include their employee ID at the end of the url.
</summary>
<br>

`http://localhost:8090/api/salespeople/:employee_id`

That salesperson's information will be returned, but their employee_id value will be null.

```
{
    "employee_id": null,
    "first_name": "Michael",
    "last_name": "Liang",
}
```
</details>

#### 2. **Customer**

| Action | Method | URL |
|--------|--------|-----|
| List Customers | GET | `http://localhost:8090/api/customers/` |
| Create a Customer | POST | `http://localhost:8090/api/customers/` |
| Delete a Customer | DELETE | `http://localhost:8090/api/customers/:id/`|

<details>
<summary>GET: Returns a list of customers and their information. The returning JSON is formatted as such:
</summary>
<br>

```
{
    "customers": [
        {
            "id": 1,
            "first_name": "Jane",
            "last_name": "Doe",
            "address": "12345 King st Akron, OH 98988",
            "phone_number": "123123123"
        },
        {
            "id": 2,
            "first_name": "Other",
            "last_name": "Customer"
            "address": "54321 Queen st Seattle, WA 98899"
            "phone_number": "321321321"
        }
    ]
}
```
</details>
<br>
<details>
<summary>POST: To create a customer, you must include a first name, last name, address and phone number in the request body.
</summary>
<br>

```
{
    "first_name": "Jane",
    "last_name": "Doe",
    "address": "12345 King st Akron, OH 98988",
    "phone_number": "123123123"
}
```
Following this request, a JSON response will be returned in a similar format, with the addition of the ID of the customer you just created.

```
{
    "id": 1,
    "first_name": "Jane",
    "last_name": "Doe",
    "address": "12345 King st Akron, OH 98988",
    "phone_number": "123123123"
}
```
</details>
<br>
<details>
<summary>DELETE: To delete a customer, you must include their ID at the end of the url.
</summary>
<br>

`http://localhost:8090/api/customers/:id`

If successfully deleted, that customer's information will be returned, but their id value will be null.

```
{
    "id": null,
    "first_name": "Jane",
    "last_name": "Doe",
    "address": "12345 King st Akron, OH 98988",
    "phone_number": "123123123"
}
```
</details>

#### 3. **Sale**

| Action | Method | URL |
|--------|--------|-----|
| List Sales | GET | `http://localhost:8090/api/sales/` |
| Create a Sale | POST | `http://localhost:8090/api/sales/` |
| Delete a Sale | DELETE | `http://localhost:8090/api/sales/:id/`|

<details>
<summary>GET: Returns a list of sales and their information. The returning JSON is formatted as such:
</summary>
<br>

```
{
    "sales": [
	    {
            "id": 1,
            "price": 30000,
            "automobile": {
                "vin": "6A5B4B36463463643",
                "id": 2
            },
            "salesperson": {
                "employee_id": 1,
                "first_name": "Mike",
                "last_name": "Liang"
            },
            "customer": {
                "id": 1,
                "first_name": "Lebron",
                "last_name": "James",
                "address": "12345 King st Akron, OH 99999",
                "phone_number": "123456789"
            }
        },
    ]
}
```

</details>
<br>
<details>
<summary>POST: To create a sale, you must include a vin of an existing automobile, an employee ID of an existing salesperson, and an ID of an existing customer. The format of your JSON request body should look as such:
</summary>
<br>

```
{
    "price": 100000,
    "automobile": "5948AD",
    "salesperson": 1,
    "customer": 1
}
```
Following this request, the information about the sale, as well as the automobile, the salesperson, and the customer associated will be returned.

```
{
	"id": 3,
	"price": 100000,
	"automobile": {
		"vin": "5948AD",
		"id": 3
	},
	"salesperson": {
		"employee_id": 1,
		"first_name": "Mike",
		"last_name": "Liang"
	},
	"customer": {
		"id": 1,
		"first_name": "Lebron",
		"last_name": "James",
		"address": "12345 King st Akron, OH 99999",
		"phone_number": "123456789"
	}
}
```
</details>
<br>
<details>
<summary>DELETE: To delete a sale, you must include its ID at the end of the url.
</summary>
<br>

`http://localhost:8090/api/sales/:id`

If successfully deleted, that sale's information will be returned, but its id value will be null.

```
{
    "id": null,
    "price": 100000,
    "automobile": {
        "vin": "5948AD",
        "id": 3
    },
    "salesperson": {
        "employee_id": 1,
        "first_name": "Mike",
        "last_name": "Liang"
    },
    "customer": {
        "id": 1,
        "first_name": "Lebron",
        "last_name": "James",
        "address": "12345 King st Akron, OH 99999",
        "phone_number": "123456789"
	}
}
```
</details>
